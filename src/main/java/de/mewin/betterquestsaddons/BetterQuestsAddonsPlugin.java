/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquestsaddons.addons.Addon;
import de.mewin.betterquestsaddons.addons.AudioConnectAddon;
import de.mewin.betterquestsaddons.addons.CitizensAddon;
import de.mewin.betterquestsaddons.addons.VaultAddon;
import de.mewin.betterquestsaddons.addons.WorldGuardAddon;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.logging.Level;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.UnknownDependencyException;
import org.bukkit.plugin.java.JavaPlugin;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class BetterQuestsAddonsPlugin extends JavaPlugin
{
    private final ArrayList<Addon> addons;
    private BetterQuestsPlugin basePlugin;

    public BetterQuestsAddonsPlugin()
    {
        this.addons = new ArrayList<>();
    }

    public BetterQuestsPlugin getBasePlugin()
    {
        return basePlugin;
    }

    @Override
    public void onEnable()
    {
        PluginManager pluginManager = getServer().getPluginManager();
        Plugin betterQuestsPlugin = pluginManager.getPlugin("BetterQuests");

        if (betterQuestsPlugin == null) {
            throw new UnknownDependencyException("missing dependency: BetterQuests");
        }

        if (!(betterQuestsPlugin instanceof BetterQuestsPlugin)) {
            throw new RuntimeException("invalid BetterQuestsPlugin plugin class");
        }

        basePlugin = (BetterQuestsPlugin) betterQuestsPlugin;
        CHAT_HELPER.loadMessages(this);

        loadAddons(pluginManager);
    }

    @Override
    public void onDisable()
    {
        unloadAddons();
    }

    private void tryLoadAddon(PluginManager pluginManager, String pluginName, Class<? extends Addon> addonClass)
    {
        Plugin plugin = pluginManager.getPlugin(pluginName);

        if (plugin != null)
        {
            getLogger().log(Level.INFO, "Detected {0}, enabling addon.", pluginName);
            try
            {
                Constructor<? extends Addon> constructor = addonClass.getConstructor(BetterQuestsAddonsPlugin.class);
                addons.add(constructor.newInstance(this));
            }
            catch(IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException ex)
            {
                getLogger().log(Level.WARNING, "Invalid addon class: {0}", addonClass.getName());
            }
        }
    }

    private void loadAddons(PluginManager pluginManager)
    {
        tryLoadAddon(pluginManager, "Vault", VaultAddon.class);
        tryLoadAddon(pluginManager, "WorldGuard", WorldGuardAddon.class);
        tryLoadAddon(pluginManager, "Citizens", CitizensAddon.class);
        tryLoadAddon(pluginManager, "AudioConnect", AudioConnectAddon.class);

        getLogger().log(Level.INFO, "Enabling a total of {0} addon(s).", addons.size());
        for (Addon addon : addons) {
            addon.onEnable();
        }
    }

    private void unloadAddons()
    {
        getLogger().info("Disabling addons.");
        for (Addon addon : addons) {
            addon.onDisable();
        }
        addons.clear();
    }
}
