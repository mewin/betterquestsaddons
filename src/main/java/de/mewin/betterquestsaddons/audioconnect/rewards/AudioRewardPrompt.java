/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.audioconnect.rewards;

import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.rewards.conversation.RewardPrompt;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class AudioRewardPrompt extends RewardPrompt
{
    private final AudioReward audioReward;

    public AudioRewardPrompt(AudioReward reward, RewardsHolder holder, MenuPrompt parentPrompt)
    {
        super(reward, holder, parentPrompt);

        this.audioReward = reward;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-audioconnect-audio-audioid", audioReward.getAudioID()),
                new StringAcceptorPrompt(this, "prompt-addons-audioconnect-audio-audioid", audioReward.getAudioIDAcceptor()));
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-audioconnect-audio-trackid", audioReward.getTrackID()),
                new StringAcceptorPrompt(this, "prompt-addons-audioconnect-audio-trackid", audioReward.getTrackIDAcceptor()));

        super.fillOptions(receiver);
    }
}
