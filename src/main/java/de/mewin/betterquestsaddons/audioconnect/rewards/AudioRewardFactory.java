/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.audioconnect.rewards;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.quest.factory.RewardFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class AudioRewardFactory extends RewardFactory
{

    @Override
    public Reward createNew(RewardsHolder holder)
    {
        AudioReward audioReward = new AudioReward(holder);
        holder.addReward(audioReward);
        return audioReward;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-audioconnect-reward-audio-factory");
    }

    @Override
    public Reward fromYaml(RewardsHolder holder, Map<String, Object> yaml) throws IOException
    {
        String audioID = "";
        String trackID = "";
        Object tmp;

        if ((tmp = yaml.get("audio")) != null) {
            audioID = tmp.toString();
        }

        if ((tmp = yaml.get("track")) != null) {
            trackID = tmp.toString();
        }

        return new AudioReward(holder, audioID, trackID);
    }

    @Override
    public Map<String, Object> toYaml(Reward reward) throws IOException
    {
        AudioReward audioReward = (AudioReward) reward;

        HashMap<String, Object> map = new HashMap<>();

        map.put("audio", audioReward.getAudioID());
        map.put("track", audioReward.getTrackID());

        return map;
    }

    public static final String MY_ID = "addons-audioconnect-audio";
}
