/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.audioconnect.rewards;

import com.deadmandungeons.audioconnect.AudioConnect;
import com.deadmandungeons.audioconnect.AudioConnectClient;
import com.deadmandungeons.audioconnect.messages.AudioMessage;
import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class AudioReward extends Reward
{
    private String audioID;
    private String trackID;

    public AudioReward(RewardsHolder holder)
    {
        super(holder);
        audioID = "";
        trackID = "";
    }

    public AudioReward(RewardsHolder holder, String audioID, String trackID)
    {
        super(holder);
        this.audioID = audioID;
        this.trackID = trackID;
    }

    public String getAudioID()
    {
        return audioID;
    }

    public void setAudioID(String audioID)
    {
        this.audioID = audioID;
    }

    public String getTrackID()
    {
        return trackID;
    }

    public void setTrackID(String trackID)
    {
        this.trackID = trackID;
    }

    @Override
    public void giveReward(Player player)
    {
        AudioConnectClient audioConnectClient = AudioConnect.getInstance().getClient();

        if (audioConnectClient.isConnected())
        {
            AudioMessage audioMessage =
                    AudioMessage.builder(player.getUniqueId())
                    .audio(audioID)
                    .track(trackID)
                    .build();
            audioConnectClient.writeAndFlush(audioMessage);
        }
    }

    @Override
    public String getMessage(Player player)
    {
        return "";
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-audioconnect-reward-audio", audioID, trackID);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RewardsHolder holder, CommandSender sender)
    {
        return new AudioRewardPrompt(this, holder, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return AudioRewardFactory.MY_ID;
    }

    public StringAcceptor getAudioIDAcceptor()
    {
        return new StringAcceptor()
        {
            @Override
            public String getString()
            {
                return audioID;
            }

            @Override
            public boolean setString(String value)
            {
                audioID = value;
                return true;
            }
        };
    }

    public StringAcceptor getTrackIDAcceptor()
    {
        return new StringAcceptor()
        {
            @Override
            public String getString()
            {
                return trackID;
            }

            @Override
            public boolean setString(String value)
            {
                trackID = value;
                return true;
            }
        };
    }
}
