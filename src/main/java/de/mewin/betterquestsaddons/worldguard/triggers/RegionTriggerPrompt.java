/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.worldguard.triggers;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.triggers.conversation.TriggerPrompt;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.EnumAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class RegionTriggerPrompt extends TriggerPrompt
{
    private final RegionTrigger regionTrigger;

    public RegionTriggerPrompt(RegionTrigger trigger, Quest quest, MenuPrompt parentPrompt)
    {
        super(trigger, quest, parentPrompt);

        regionTrigger = trigger;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-worldguard-region", regionTrigger.getRegion()),
                new StringAcceptorPrompt(this, "prompt-addons-worldguard-region", regionTrigger));
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-worldguard-region-trigger-type", regionTrigger.getType().toString()),
                new EnumAcceptorPrompt(this, "prompt-addons-worldguard-region-trigger-type", regionTrigger, RegionTrigger.Type.class));
        super.fillOptions(receiver);
    }
}
