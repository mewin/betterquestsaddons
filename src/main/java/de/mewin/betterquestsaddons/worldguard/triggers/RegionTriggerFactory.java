/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.worldguard.triggers;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.quest.factory.TriggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class RegionTriggerFactory extends TriggerFactory
{
    @Override
    public Trigger createNew(Quest base)
    {
        RegionTrigger trigger = new RegionTrigger();
        base.addTrigger(trigger);
        return trigger;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-worldguard-trigger-region-factory");
    }

    @Override
    public Trigger fromYaml(Quest quest, Map<String, Object> yaml) throws IOException
    {
        Object tmp;
        String region = "";
        RegionTrigger.Type type = RegionTrigger.Type.ENTER;

        if ((tmp = yaml.get("region")) != null) {
            region = tmp.toString();
        }
        if ((tmp = yaml.get("listener-type")) != null)
        {
            try {
                type = RegionTrigger.Type.valueOf(tmp.toString());
            } catch(IllegalArgumentException ex) {}
        }
        return new RegionTrigger(region, type);
    }

    @Override
    public Map<String, Object> toYaml(Trigger trigger) throws IOException
    {
        RegionTrigger regionTrigger = (RegionTrigger) trigger;

        HashMap<String, Object> map = new HashMap<>();
        map.put("region", regionTrigger.getRegion());
        map.put("listener-type", regionTrigger.getType().toString());

        return map;
    }

    public static final String MY_ID = "addons-worldguard-region";
}
