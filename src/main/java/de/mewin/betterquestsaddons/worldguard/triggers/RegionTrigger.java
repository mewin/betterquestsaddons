/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.worldguard.triggers;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.EnumAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class RegionTrigger extends Trigger implements StringAcceptor, EnumAcceptor<RegionTrigger.Type>
{
    private String region;
    private Type type;

    public RegionTrigger()
    {
        region = "";
        type = Type.ENTER;
    }

    public RegionTrigger(String region, Type type)
    {
        this.region = region;
        this.type = type;
    }

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-worldguard-trigger-region", type.name(), region);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, Quest base, CommandSender sender)
    {
        return new RegionTriggerPrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return RegionTriggerFactory.MY_ID;
    }

    @Override
    public String getString()
    {
        return region;
    }

    @Override
    public boolean setString(String value)
    {
        region = value;
        return true;
    }

    @Override
    public Type getEnum()
    {
        return type;
    }

    @Override
    public boolean setEnum(Type value)
    {
        type = value;
        return true;
    }

    public static enum Type
    {
        ENTER,
        LEAVE
    }
}
