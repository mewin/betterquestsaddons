/*
 * Copyright (c) 2018 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.worldguard.requirements;

import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.requirements.conversation.RequirementPrompt;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class RegionRequirementPrompt extends RequirementPrompt
{
    private final RegionRequirement regionRequirement;

    public RegionRequirementPrompt(RegionRequirement requirement, RequirementsHolder holder, MenuPrompt parentPrompt)
    {
        super(requirement, holder, parentPrompt);

        regionRequirement = requirement;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-worldguard-region", regionRequirement.getRegion()),
                new StringAcceptorPrompt(this, "prompt-addons-worldguard-region", regionRequirement));
        super.fillOptions(receiver);
    }
}
