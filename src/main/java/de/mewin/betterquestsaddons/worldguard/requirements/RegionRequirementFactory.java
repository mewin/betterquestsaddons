/*
 * Copyright (c) 2018 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.worldguard.requirements;

import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.quest.factory.RequirementFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class RegionRequirementFactory extends RequirementFactory
{
    @Override
    public Requirement createNew(RequirementsHolder holder)
    {
        RegionRequirement requirement = new RegionRequirement(holder);
        holder.addRequirement(requirement);
        return requirement;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-worldguard-requirement-region-factory");
    }

    @Override
    public Requirement fromYaml(RequirementsHolder holder, Map<String, Object> yaml) throws IOException
    {
        String region = "";
        Object tmp;

        if ((tmp = yaml.get("region")) != null) {
            region = tmp.toString();
        }

        return new RegionRequirement(holder, region);
    }

    @Override
    public Map<String, Object> toYaml(Requirement requirement) throws IOException
    {
        RegionRequirement regionRequirement = (RegionRequirement) requirement;

        HashMap<String, Object> map = new HashMap<>();

        map.put("region", regionRequirement.getRegion());

        return map;
    }

    public static final String MY_ID = "addons-worldguard-region";
}
