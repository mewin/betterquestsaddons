/*
 * Copyright (c) 2018 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.worldguard.requirements;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class RegionRequirement extends Requirement implements StringAcceptor
{
    private String region;

    public RegionRequirement(RequirementsHolder holder)
    {
        super(holder);
        this.region = "";
    }

    public RegionRequirement(RequirementsHolder holder, String region)
    {
        super(holder);
        this.region = region;
    }

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }

    @Override
    public boolean testRequirement(Player player)
    {
        Location loc = player.getLocation();
        WorldGuardPlugin wgPlugin = WorldGuardPlugin.inst();
        RegionManager regionManager = wgPlugin.getRegionManager(loc.getWorld());

        if (regionManager == null) {
            return false;
        }

        ApplicableRegionSet regions = regionManager.getApplicableRegions(loc);

        for (ProtectedRegion rgn : regions.getRegions())
        {
            if (rgn.getId().equals(region)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String getFailMessage(Player player)
    {
        return CHAT_HELPER.formatMessage(player, "addons-worldguard-requirement-region-missing", region);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-worldguard-requirement-region", region);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RequirementsHolder base, CommandSender sender)
    {
        return new RegionRequirementPrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return RegionRequirementFactory.MY_ID;
    }

    @Override
    public boolean setString(String value)
    {
        region = value;
        return true;
    }

    @Override
    public String getString()
    {
        return region;
    }

}
