/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.worldguard.objectives;

import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class RegionObjectiveFactory extends ObjectiveFactory
{
    @Override
    public Objective createNew(ObjectivesHolder holder)
    {
        RegionObjective objective = new RegionObjective(holder);
        holder.addObjective(objective);
        return objective;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-worldguard-objective-region-factory");
    }

    @Override
    public Objective fromYaml(ObjectivesHolder holder, Map<String, Object> yaml) throws IOException
    {
        String region = "";
        RegionObjective.Type type = RegionObjective.Type.ENTER;
        Object tmp;

        if ((tmp = yaml.get("region")) != null) {
            region = tmp.toString();
        }

        if ((tmp = yaml.get("listener-type")) != null)
        {
            try {
                type = RegionObjective.Type.valueOf(tmp.toString());
            } catch(IllegalArgumentException ex) {}
        }

        return new RegionObjective(holder, region, type);
    }

    @Override
    public Map<String, Object> toYaml(Objective objective) throws IOException
    {
        RegionObjective regionObjective = (RegionObjective) objective;

        HashMap<String, Object> map = new HashMap<>();

        map.put("region", regionObjective.getRegion());
        map.put("listener-type", regionObjective.getType().name());

        return map;
    }

    public static final String MY_ID = "addons-worldguard-region";
}
