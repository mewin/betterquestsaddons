/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.worldguard.objectives;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.EnumAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class RegionObjective extends Objective implements StringAcceptor, EnumAcceptor<RegionObjective.Type>
{
    private String region;
    private Type type;

    public RegionObjective(ObjectivesHolder holder)
    {
        super(holder);
        region = "";
        type = Type.ENTER;
    }

    public RegionObjective(ObjectivesHolder holder, String region, Type type)
    {
        super(holder);
        this.region = region;
        this.type = type;
    }

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    private boolean testPlayerInside(Player player)
    {
        Location loc = player.getLocation();
        WorldGuardPlugin wgPlugin = WorldGuardPlugin.inst();
        RegionManager regionManager = wgPlugin.getRegionManager(loc.getWorld());

        if (regionManager == null) {
            return false;
        }

        ApplicableRegionSet regions = regionManager.getApplicableRegions(loc);

        for (ProtectedRegion rgn : regions.getRegions())
        {
            if (rgn.getId().equals(region)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public Location getTargetLocation()
    {
        return null; // TODO: we have to store the world to find the regions location
    }

    @Override
    public boolean testObjective(Player player, Object data)
    {
        switch (type)
        {
            case ENTER:
            case LEAVE:
                return data != null
                        && (data instanceof Boolean)
                        && (Boolean) data;
            case BE_INSIDE:
                return testPlayerInside(player);
            case BE_OUTSIDE:
                return !testPlayerInside(player);
            default:
                return true;
        }
    }

    @Override
    public String getDescription(Player player, Object data)
    {
        switch (type)
        {
            case ENTER:
                return CHAT_HELPER.getMessage(player, "addons-worldguard-objective-region-description-enter", region);
            case LEAVE:
                return CHAT_HELPER.getMessage(player, "addons-worldguard-objective-region-description-leave", region);
            case BE_INSIDE:
                return CHAT_HELPER.getMessage(player, "addons-worldguard-objective-region-description-be-inside", region);
            case BE_OUTSIDE:
                return CHAT_HELPER.getMessage(player, "addons-worldguard-objective-region-description-be-outside", region);
            case DONT_ENTER:
                return CHAT_HELPER.getMessage(player, "addons-worldguard-objective-region-description-dont-enter", region);
            case DONT_LEAVE:
                return CHAT_HELPER.getMessage(player, "addons-worldguard-objective-region-description-dont-leave", region);
        }

        return "";
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-worldguard-objective-region", type.name(), region);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, ObjectivesHolder base, CommandSender sender)
    {
        return new RegionObjectivePrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return RegionObjectiveFactory.MY_ID;
    }

    @Override
    public String getString()
    {
        return region;
    }

    @Override
    public boolean setString(String value)
    {
        region = value;
        return true;
    }

    @Override
    public Type getEnum()
    {
        return type;
    }

    @Override
    public boolean setEnum(Type value)
    {
        type = value;
        return true;
    }

    public static enum Type
    {
        ENTER,
        LEAVE,
        BE_INSIDE,
        BE_OUTSIDE,
        DONT_ENTER,
        DONT_LEAVE
    }
}
