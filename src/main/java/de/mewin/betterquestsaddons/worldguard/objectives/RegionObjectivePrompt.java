/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.worldguard.objectives;

import de.mewin.betterquests.objectives.conversation.ObjectivePrompt;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.EnumAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class RegionObjectivePrompt extends ObjectivePrompt
{
    private final RegionObjective regionTrigger;

    public RegionObjectivePrompt(RegionObjective objective, ObjectivesHolder holder, MenuPrompt parentPrompt)
    {
        super(objective, holder, parentPrompt);

        regionTrigger = objective;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-worldguard-region", regionTrigger.getRegion()),
                new StringAcceptorPrompt(this, "prompt-addons-worldguard-region", regionTrigger));
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-worldguard-region-objective-type", regionTrigger.getType().toString()),
                new EnumAcceptorPrompt(this, "prompt-addons-worldguard-region-objective-type", regionTrigger, RegionObjective.Type.class));
        super.fillOptions(receiver);
    }
}
