/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.worldguard;

import com.mewin.WGRegionEvents.events.RegionEnteredEvent;
import com.mewin.WGRegionEvents.events.RegionLeftEvent;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.ObjectsByClassRegistry;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.mewinbukkitlib.util.Pair;
import de.mewin.betterquestsaddons.worldguard.objectives.RegionObjective;
import de.mewin.betterquestsaddons.worldguard.triggers.RegionTrigger;
import java.util.List;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author mewin
 */
public class RegionListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public RegionListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    private void testTriggers(Player player, String region, RegionTrigger.Type type)
    {
        ObjectsByClassRegistry<Trigger> triggerRegistry = plugin.getTriggerRegistry();
        ProgressManager progressManager = plugin.getProgressManager();

        List<Trigger> triggers = triggerRegistry.getRegistered(RegionTrigger.class);

        for (Trigger trigger : triggers)
        {
            RegionTrigger regionTrigger = (RegionTrigger) trigger;

            if (regionTrigger.getRegion().equals(region)
                    && regionTrigger.getType() == type) {
                progressManager.advertiseQuest(player, trigger.getQuest(), trigger);
            }
        }
    }

    private void testObjectives(Player player, String region, boolean entered)
    {
        ProgressManager progressManager = plugin.getProgressManager();

        List<Pair<QuestProgress, RegionObjective>> objectives = progressManager.findProgressByObjectiveType(player, RegionObjective.class);

        for (Pair<QuestProgress, RegionObjective> ele : objectives)
        {
            QuestProgress progress = ele.getFirst();
            RegionObjective objective = ele.getSecond();

            if (!objective.getRegion().equals(region)) {
                continue;
            }

            if (entered)
            {
                switch(objective.getType())
                {
                    case ENTER:
                        progress.setObjectiveProgress(objective, true);
                        break;
                    case DONT_ENTER:
                        progressManager.failQuest(player, progress);
                        return;
                }
            }
            else
            {
                switch (objective.getType())
                {
                    case LEAVE:
                        progress.setObjectiveProgress(objective, true);
                        break;
                    case DONT_LEAVE:
                        progressManager.failQuest(player, progress);
                        return;
                }
            }

            progressManager.updateQuestProgress(player, progress);
        }
    }

    @EventHandler
    public void onRegionEntered(RegionEnteredEvent event)
    {
        ProtectedRegion region = event.getRegion();
        Player player = event.getPlayer();

        testTriggers(player, region.getId(), RegionTrigger.Type.ENTER);
        testObjectives(player, region.getId(), true);
    }

    @EventHandler
    public void onRegionLeft(RegionLeftEvent event)
    {
        ProtectedRegion region = event.getRegion();
        Player player = event.getPlayer();

        testTriggers(player, region.getId(), RegionTrigger.Type.LEAVE);
        testObjectives(player, region.getId(), false);
    }
}
