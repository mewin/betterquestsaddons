/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.citizens.objectives;

import de.mewin.betterquests.conversation.editors.EditRequirementsPrompt;
import de.mewin.betterquests.objectives.conversation.ObjectivePrompt;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.extra.DataType;
import de.mewin.betterquests.quest.extra.KnownSetting;
import de.mewin.betterquestsaddons.citizens.triggers.NPCPrompt;
import java.util.ArrayList;
import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class NPCObjectivePrompt extends ObjectivePrompt
{
    private final NPCObjective npcObjective;

    public NPCObjectivePrompt(NPCObjective objective, ObjectivesHolder holder, MenuPrompt parentPrompt)
    {
        super(objective, holder, parentPrompt);

        this.npcObjective = objective;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        int npcid = npcObjective.getNPCid();
        NPC npc = npcid >= 0 ? npcRegistry.getById(npcid) : null;
        String name = npc == null ? "???" : npc.getName();

        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-citizens-npc", npcid, name),
                new NPCPrompt(this, "prompt-addons-citizens-npc", npcObjective));
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-edit-requirements"),
                new EditRequirementsPrompt(npcObjective, this, receiver));

        super.fillOptions(receiver);
    }

    @Override
    protected List<KnownSetting> getKnownSettings()
    {
        ArrayList<KnownSetting> settings = new ArrayList<>(super.getKnownSettings());

        settings.add(new KnownSetting(NPCObjective.DELIVERY_MESSAGE, DataType.LONG_STRING));

        return settings;
    }
}
