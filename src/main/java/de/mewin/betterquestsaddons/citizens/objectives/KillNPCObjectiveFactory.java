/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.citizens.objectives;

import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class KillNPCObjectiveFactory extends ObjectiveFactory
{
    @Override
    public Objective createNew(ObjectivesHolder holder)
    {
        KillNPCObjective objective = new KillNPCObjective(holder);
        holder.addObjective(objective);
        return objective;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-citizens-objective-kill-npc-factory");
    }

    @Override
    public Objective fromYaml(ObjectivesHolder holder, Map<String, Object> yaml) throws IOException
    {
        int npcid = -1;
        Object tmp;

        if ((tmp = yaml.get("npcid")) != null && (tmp instanceof Number)) {
            npcid = ((Number) tmp).intValue();
        }

        return new KillNPCObjective(holder, npcid);
    }

    @Override
    public Map<String, Object> toYaml(Objective objective) throws IOException
    {
        KillNPCObjective killNPCObjective = (KillNPCObjective) objective;

        HashMap<String, Object> map = new HashMap<>();

        map.put("npcid", killNPCObjective.getNPCid());

        return map;
    }

    public static final String MY_ID = "addons-kill-npc";
}
