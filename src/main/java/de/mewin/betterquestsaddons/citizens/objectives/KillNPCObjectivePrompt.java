/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.citizens.objectives;

import de.mewin.betterquests.objectives.conversation.ObjectivePrompt;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquestsaddons.citizens.triggers.NPCPrompt;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class KillNPCObjectivePrompt extends ObjectivePrompt
{
    private final KillNPCObjective killNPCObjective;

    public KillNPCObjectivePrompt(KillNPCObjective objective, ObjectivesHolder holder, MenuPrompt parentPrompt)
    {
        super(objective, holder, parentPrompt);

        this.killNPCObjective = objective;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        int npcid = killNPCObjective.getNPCid();
        NPC npc = npcid >= 0 ? npcRegistry.getById(npcid) : null;
        String name = npc == null ? "???" : npc.getName();

        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-citizens-npc", npcid, name),
                new NPCPrompt(this, "prompt-addons-citizens-npc", killNPCObjective));

        super.fillOptions(receiver);
    }
}
