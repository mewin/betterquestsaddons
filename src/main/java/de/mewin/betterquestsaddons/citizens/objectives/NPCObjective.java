/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.citizens.objectives;

import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptor;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class NPCObjective extends Objective implements RequirementsHolder, IntAcceptor
{
    private final ArrayList<Requirement> requirements;
    private int npcid;

    public NPCObjective(ObjectivesHolder holder)
    {
        super(holder);

        requirements = new ArrayList<>();
        npcid = -1;
    }


    public NPCObjective(ObjectivesHolder holder, int npcid)
    {
        super(holder);

        requirements = new ArrayList<>();
        this.npcid = npcid;
    }

    public int getNPCid()
    {
        return npcid;
    }

    public void setNPCid(int npcid)
    {
        this.npcid = npcid;
    }

    @Override
    public Location getTargetLocation()
    {
        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        NPC npc = npcid >= 0 ? npcRegistry.getById(npcid) : null;

        return npc == null ? null : npc.getStoredLocation();
    }

    @Override
    public boolean testObjective(Player player, Object data)
    {
        return data != null
                && (data instanceof Boolean)
                && (Boolean) data;
    }

    @Override
    public String getDescription(Player player, Object data)
    {
        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        NPC npc = npcid >= 0 ? npcRegistry.getById(npcid) : null;
        String name = npc == null ? "???" : npc.getName();

        return CHAT_HELPER.getMessage(player, "addons-citizens-objective-npc-description", name);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        NPC npc = npcid >= 0 ? npcRegistry.getById(npcid) : null;
        String name = npc == null ? "???" : npc.getName();

        return CHAT_HELPER.getMessage(sender, "addons-citizens-objective-npc", npcid, name);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, ObjectivesHolder base, CommandSender sender)
    {
        return new NPCObjectivePrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return NPCObjectiveFactory.MY_ID;
    }

    @Override
    public void addRequirement(Requirement requirement)
    {
        requirements.add(requirement);
        // Bukkit.getPluginManager().callEvent(new RequirementAddedEvent(getStage().getQuest(), getStage(), objective));
    }

    @Override
    public void removeRequirement(Requirement requirement)
    {
        if (requirements.remove(requirement)) {
            requirement.onRemove();
        }
    }

    @Override
    public List<Requirement> getRequirements()
    {
        return Collections.unmodifiableList(requirements);
    }

    @Override
    public Quest getQuest()
    {
        return getHolder().getStage().getQuest();
    }

    @Override
    public boolean setInt(int value)
    {
        if (value < -1) {
            npcid = -1;
        } else {
            npcid = value;
        }
        return true;
    }

    @Override
    public int getInt()
    {
        return npcid;
    }

    public static final String DELIVERY_MESSAGE = "delivery-message";
}
