/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.citizens.objectives;

import de.mewin.betterquests.io.YamlQuestRegistryBackend;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class NPCObjectiveFactory extends ObjectiveFactory
{
    @Override
    public Objective createNew(ObjectivesHolder holder)
    {
        NPCObjective objective = new NPCObjective(holder);
        holder.addObjective(objective);
        return objective;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-citizens-objective-npc-factory");
    }

    @Override
    public Objective fromYaml(ObjectivesHolder holder, Map<String, Object> yaml) throws IOException
    {
        int npcid = -1;
        Object tmp;

        if ((tmp = yaml.get("npcid")) != null && (tmp instanceof Number)) {
            npcid = ((Number) tmp).intValue();
        }

        NPCObjective objective = new NPCObjective(holder, npcid);

        if ((tmp = yaml.get("requirements")) != null && (tmp instanceof List))
        {
            Quest quest = holder.getStage().getQuest();
            List list = (List) tmp;

            for (Object ele : list)
            {
                if (ele instanceof Map)
                {
                    Requirement requirement = YamlQuestRegistryBackend.requirementFromYaml(quest, (Map) ele);
                    objective.addRequirement(requirement);
                }
            }
        }

        return objective;
    }

    @Override
    public Map<String, Object> toYaml(Objective objective) throws IOException
    {
        NPCObjective npcObjective = (NPCObjective) objective;

        HashMap<String, Object> map = new HashMap<>();

        map.put("npcid", npcObjective.getNPCid());

        ArrayList list = new ArrayList();
        for (Requirement requirement : npcObjective.getRequirements()) {
            list.add(YamlQuestRegistryBackend.requirementToYaml(requirement));
        }
        map.put("requirements", list);

        return map;
    }

    public static final String MY_ID = "addons-npc";
}
