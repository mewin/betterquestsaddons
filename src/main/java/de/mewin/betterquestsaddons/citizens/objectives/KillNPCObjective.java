/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.citizens.objectives;

import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptor;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class KillNPCObjective extends Objective implements IntAcceptor
{
    private int npcid;

    public KillNPCObjective(ObjectivesHolder holder)
    {
        super(holder);
    }

    public KillNPCObjective(ObjectivesHolder holder, int npcid)
    {
        super(holder);
        this.npcid = npcid;
    }

    public int getNPCid()
    {
        return npcid;
    }

    public void setNPCid(int npcid)
    {
        this.npcid = npcid;
    }

    @Override
    public Location getTargetLocation()
    {
        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        NPC npc = npcid >= 0 ? npcRegistry.getById(npcid) : null;

        return npc == null ? null : npc.getStoredLocation();
    }

    @Override
    public boolean testObjective(Player player, Object data)
    {
        return data != null && (data instanceof Boolean) && (Boolean) data;
    }

    @Override
    public String getDescription(Player player, Object data)
    {
        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        NPC npc = npcid >= 0 ? npcRegistry.getById(npcid) : null;
        String name = npc == null ? "???" : npc.getName();

        return CHAT_HELPER.getMessage(player, "addons-citizens-objective-kill-npc-description", name);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        NPC npc = npcid >= 0 ? npcRegistry.getById(npcid) : null;
        String name = npc == null ? "???" : npc.getName();

        return CHAT_HELPER.getMessage(sender, "addons-citizens-objective-kill-npc", npcid, name);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, ObjectivesHolder base, CommandSender sender)
    {
        return new KillNPCObjectivePrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return KillNPCObjectiveFactory.MY_ID;
    }

    @Override
    public boolean setInt(int value)
    {
        if (value < -1) {
            npcid = -1;
        } else {
            npcid = value;
        }
        return true;
    }

    @Override
    public int getInt()
    {
        return npcid;
    }
}
