/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.citizens.triggers;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.quest.factory.TriggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class NPCTriggerFactory extends TriggerFactory
{
    @Override
    public Trigger createNew(Quest quest)
    {
        NPCTrigger trigger = new NPCTrigger();
        quest.addTrigger(trigger);
        return trigger;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-citizens-trigger-npc-factory");
    }

    @Override
    public Trigger fromYaml(Quest quest, Map<String, Object> yaml) throws IOException
    {
        int npcid = -1;

        Object tmp;

        if ((tmp = yaml.get("npcid")) != null && (tmp instanceof Number)) {
            npcid = ((Number) tmp).intValue();
        }

        return new NPCTrigger(npcid);
    }

    @Override
    public Map<String, Object> toYaml(Trigger trigger) throws IOException
    {
        NPCTrigger npcTrigger = (NPCTrigger) trigger;

        HashMap<String, Object> map = new HashMap<>();

        map.put("npcid", npcTrigger.getNPCid());

        return map;
    }

    public static final String MY_ID = "addons-citizens-npc";
}
