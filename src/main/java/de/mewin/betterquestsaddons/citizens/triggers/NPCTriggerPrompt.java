/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.citizens.triggers;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.triggers.conversation.TriggerPrompt;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class NPCTriggerPrompt extends TriggerPrompt
{
    private final NPCTrigger npcTrigger;

    public NPCTriggerPrompt(NPCTrigger trigger, Quest quest, MenuPrompt parentPrompt)
    {
        super(trigger, quest, parentPrompt);

        this.npcTrigger = trigger;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        int npcid = npcTrigger.getNPCid();
        NPC npc = npcid >= 0 ? npcRegistry.getById(npcid) : null;
        String name = npc == null ? "???" : npc.getName();

        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-citizens-npc", npcid, name),
                new NPCPrompt(this, "prompt-addons-citizens-npc", npcTrigger));

        super.fillOptions(receiver);
    }
}
