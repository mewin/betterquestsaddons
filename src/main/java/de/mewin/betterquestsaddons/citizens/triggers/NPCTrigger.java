/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.citizens.triggers;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptor;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class NPCTrigger extends Trigger implements IntAcceptor
{
    private int npcid;

    public NPCTrigger()
    {
        npcid = -1;
    }

    public NPCTrigger(int npcid)
    {
        this.npcid = npcid;
    }

    public int getNPCid()
    {
        return npcid;
    }

    public void setNPCid(int npcid)
    {
        this.npcid = npcid;
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        NPC npc = npcid >= 0 ? npcRegistry.getById(npcid) : null;
        String name = npc == null ? "???" : npc.getName();
        return CHAT_HELPER.getMessage(sender, "addons-citizens-trigger-npc", npcid, name);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, Quest base, CommandSender sender)
    {
        return new NPCTriggerPrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return NPCTriggerFactory.MY_ID;
    }

    @Override
    public Location getLocation()
    {
        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        NPC npc = npcid >= 0 ? npcRegistry.getById(npcid) : null;

        return npc == null ? null : npc.getStoredLocation();
    }

    @Override
    public int getInt()
    {
        return npcid;
    }

    @Override
    public boolean setInt(int value)
    {
        npcid = value;
        return true;
    }
}
