/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.citizens.triggers;

import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptorPrompt;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

/**
 *
 * @author mewin
 */
public class NPCPrompt extends IntAcceptorPrompt
{
    public NPCPrompt(Prompt parentPrompt, String promptid, IntAcceptor acceptor)
    {
        super(parentPrompt, promptid, acceptor);
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        context.setSessionData(NPCPrompt.IN_NPC_PROMPT, true);
        return super.getPromptText(context);
    }

    @Override
    protected Prompt acceptValidatedInput(ConversationContext context, Number input)
    {
        Prompt result = super.acceptValidatedInput(context, input);

        if (result != this) {
            context.setSessionData(NPCPrompt.IN_NPC_PROMPT, false);
        }

        return result;
    }

    public static final String IN_NPC_PROMPT=  "in-npc-prompt";
}
