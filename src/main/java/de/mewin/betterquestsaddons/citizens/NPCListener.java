/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.citizens;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.ObjectsByClassRegistry;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.mewinbukkitlib.util.Pair;
import de.mewin.betterquestsaddons.citizens.objectives.KillNPCObjective;
import de.mewin.betterquestsaddons.citizens.objectives.NPCObjective;
import de.mewin.betterquestsaddons.citizens.triggers.NPCPrompt;
import de.mewin.betterquestsaddons.citizens.triggers.NPCTrigger;
import java.util.List;
import net.citizensnpcs.api.event.NPCDeathEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.projectiles.ProjectileSource;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;
import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CONVERSATION_MANAGER;

/**
 *
 * @author mewin
 */
public class NPCListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public NPCListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onNPCRightClick(NPCRightClickEvent event)
    {
        NPC npc = event.getNPC();
        Player player = event.getClicker();

        Conversation conversation = CONVERSATION_MANAGER.getCurrentConversation(player);
        if (conversation != null)
        {
            ConversationContext context = conversation.getContext();
            Object tmp;
            if ((tmp = context.getSessionData(NPCPrompt.IN_NPC_PROMPT)) != null
                    && tmp instanceof Boolean
                    && (Boolean) tmp) {
                conversation.acceptInput(String.valueOf(npc.getId()));
            }
            return;
        }

        ProgressManager progressManager = plugin.getProgressManager();

        // triggers first, so quests wont instantly start after returning to npc
        ObjectsByClassRegistry<Trigger> triggerRegistry = plugin.getTriggerRegistry();
        List<Trigger> triggers = triggerRegistry.getRegistered(NPCTrigger.class);

        for (Trigger trigger : triggers)
        {
            NPCTrigger npcTrigger = (NPCTrigger) trigger;

            if (npcTrigger.getNPCid() == npc.getId()) {
                progressManager.advertiseQuest(player, npcTrigger.getQuest(), npcTrigger);
            }
        }

        List<Pair<QuestProgress, NPCObjective>> objectives = progressManager.findProgressByObjectiveType(player, NPCObjective.class);

        for (Pair<QuestProgress, NPCObjective> ele : objectives)
        {
            QuestProgress progress = ele.getFirst();
            NPCObjective objective = ele.getSecond();
            if (objective.getNPCid() == npc.getId()
                    && progressManager.processRequirements(player, objective, true))
            {
                String msg = objective.getSettings().getString(NPCObjective.DELIVERY_MESSAGE, "");
                if (!"".equals(msg)) {
                    CHAT_HELPER.sendUserMessage(player, msg);
                }
                progressManager.applyRequirements(player, objective);
                progress.setObjectiveProgress(objective, true);
                progressManager.updateQuestProgress(player, progress);
            }
        }
    }

    private void onNPCKilledByPlayer(NPC npc, Player player)
    {
        ProgressManager progressManager = plugin.getProgressManager();
        List<Pair<QuestProgress, KillNPCObjective>> progresses = progressManager.findProgressByObjectiveType(player, KillNPCObjective.class);

        for (Pair<QuestProgress, KillNPCObjective> pair : progresses)
        {
            QuestProgress progress = pair.getFirst();
            KillNPCObjective objective = pair.getSecond();

            if (objective.getNPCid()!= npc.getId()) {
                continue;
            }

            progress.setObjectiveProgress(objective, true);

            progressManager.updateQuestProgress(player, progress);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onNPCDeath(NPCDeathEvent event)
    {
        NPC npc = event.getNPC();
        Entity entity = npc.getEntity();
        EntityDamageEvent lastDamageCause = entity.getLastDamageCause();

        if (lastDamageCause == null || lastDamageCause.isCancelled()
                || !(lastDamageCause instanceof EntityDamageByEntityEvent)) {
            return;
        }

        EntityDamageByEntityEvent dmgEvent = (EntityDamageByEntityEvent) lastDamageCause;
        Entity damager = dmgEvent.getDamager();

        if (damager instanceof Player) {
            onNPCKilledByPlayer(npc, (Player) damager);
        }
        else if (damager instanceof Projectile)
        {
            Projectile projectile = (Projectile) damager;
            ProjectileSource shooter = projectile.getShooter();

            if (shooter instanceof Player) {
                onNPCKilledByPlayer(npc, (Player) shooter);
            }
        }
        else if (damager instanceof TNTPrimed)
        {
            TNTPrimed tnt = (TNTPrimed) damager;
            Entity source = tnt.getSource();

            if (source instanceof Player) {
                onNPCKilledByPlayer(npc, (Player) source);
            }
        }
    }
}
