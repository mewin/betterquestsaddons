/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.addons;

import de.mewin.betterquests.quest.FactoryRegistry;
import de.mewin.betterquestsaddons.BetterQuestsAddonsPlugin;
import de.mewin.betterquestsaddons.worldguard.triggers.RegionTriggerFactory;
import de.mewin.betterquestsaddons.worldguard.RegionListener;
import de.mewin.betterquestsaddons.worldguard.objectives.RegionObjectiveFactory;
import de.mewin.betterquestsaddons.worldguard.requirements.RegionRequirementFactory;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author mewin
 */
public class WorldGuardAddon extends Addon
{
    public WorldGuardAddon(BetterQuestsAddonsPlugin plugin)
    {
        super(plugin);
    }

    @Override
    public void onEnable()
    {
        FactoryRegistry factoryRegistry = betterQuests.getFactoryRegistry();

        Plugin wgRegionEventsPlugin = Bukkit.getPluginManager().getPlugin("WGRegionEvents");

        if (wgRegionEventsPlugin == null) {
            betterQuestsAddons.getLogger().warning("WGRegionEvents not installed, some functionality will not be available.");
        }
        else
        {
            factoryRegistry.tryRegisterTriggerFactory(RegionTriggerFactory.MY_ID, new RegionTriggerFactory());
            factoryRegistry.tryRegisterRequirementFactory(RegionRequirementFactory.MY_ID, new RegionRequirementFactory());
            factoryRegistry.tryRegisterObjectiveFactory(RegionObjectiveFactory.MY_ID, new RegionObjectiveFactory());

            Bukkit.getPluginManager().registerEvents(new RegionListener(betterQuests), betterQuestsAddons);
        }
    }
}
