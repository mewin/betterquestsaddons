/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.addons;

import de.mewin.betterquestsaddons.BetterQuestsAddonsPlugin;

/**
 *
 * @author mewin
 */
public class DenizenAddon extends Addon
{
    public DenizenAddon(BetterQuestsAddonsPlugin plugin)
    {
        super(plugin);
    }

    @Override
    public void onEnable()
    {
        
    }
}
