/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.addons;

import de.mewin.betterquests.ObjectsByClassRegistry;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.quest.FactoryRegistry;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.quest.extra.RequirementSettings;
import de.mewin.betterquestsaddons.BetterQuestsAddonsPlugin;
import de.mewin.betterquestsaddons.citizens.triggers.NPCTrigger;
import de.mewin.betterquestsaddons.citizens.triggers.NPCTriggerFactory;
import de.mewin.betterquestsaddons.citizens.NPCListener;
import de.mewin.betterquestsaddons.citizens.objectives.KillNPCObjectiveFactory;
import de.mewin.betterquestsaddons.citizens.objectives.NPCObjectiveFactory;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

/**
 *
 * @author mewin
 */
public class CitizensAddon extends Addon
{
    public CitizensAddon(BetterQuestsAddonsPlugin plugin)
    {
        super(plugin);
    }

    @Override
    public void onEnable()
    {
        FactoryRegistry factoryRegistry = betterQuests.getFactoryRegistry();

        factoryRegistry.tryRegisterTriggerFactory(NPCTriggerFactory.MY_ID, new NPCTriggerFactory());
        factoryRegistry.tryRegisterObjectiveFactory(NPCObjectiveFactory.MY_ID, new NPCObjectiveFactory());
        factoryRegistry.tryRegisterObjectiveFactory(KillNPCObjectiveFactory.MY_ID, new KillNPCObjectiveFactory());

        Bukkit.getPluginManager().registerEvents(new NPCListener(betterQuests), betterQuestsAddons);

        Bukkit.getScheduler().runTaskTimer(betterQuestsAddons, genNPCParticleTask(), 20, 20); // TODO: configurable delay
    }

    private void spawnNPCParticles(int npcid, HashSet<Quest> quests)
    {
        if (npcid < 0) {
            return;
        }

        NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
        NPC npc = npcRegistry.getById(npcid);

        if (npc == null || !npc.isSpawned()) {
            return;
        }

        Entity entity = npc.getEntity();
        Location location;
        if (entity instanceof HumanEntity) {
            location = ((HumanEntity) entity).getEyeLocation();
        }
        else
        {
            location = entity.getLocation();
            location.add(0.0, 1.0, 0.0);
        }

        ProgressManager progressManager = betterQuests.getProgressManager();
        World world = location.getWorld();

        for (Player player : world.getPlayers())
        {
            if (player.getLocation().distanceSquared(location) > 100.0) { // TODO: make configurable
                continue;
            }

            for (Quest quest : quests)
            {
                if (progressManager.isQuestAvailable(player, quest))
                {
                    if (progressManager.isQuestSilenced(player, quest)) {
                        continue;
                    }
                    player.spawnParticle(Particle.NOTE, location, 1);
                    break;
                }
            }
        }
   }

    private void spawnNPCParticles()
    {
        ObjectsByClassRegistry<Trigger> triggerRegistry = betterQuests.getTriggerRegistry();
        List<Trigger> triggers = triggerRegistry.getRegistered(NPCTrigger.class);
        HashMap<Integer, HashSet<Quest>> npcQuests = new HashMap<>();

        for (Trigger trigger : triggers)
        {
            NPCTrigger npcTrigger = (NPCTrigger) trigger;

            int npcid = npcTrigger.getNPCid();
            if (!npcQuests.containsKey(npcid)) {
                npcQuests.put(npcid, new HashSet<Quest>());
            }
            npcQuests.get(npcid).add(npcTrigger.getQuest());
        }

        for (Map.Entry<Integer, HashSet<Quest>> entry : npcQuests.entrySet())
        {
            spawnNPCParticles(entry.getKey(), entry.getValue());
        }
    }

    private Runnable genNPCParticleTask()
    {
        return new Runnable()
        {
            @Override
            public void run()
            {
                spawnNPCParticles();
            }
        };
    }
}
