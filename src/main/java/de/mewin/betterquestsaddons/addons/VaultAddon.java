/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.addons;

import de.mewin.betterquests.quest.FactoryRegistry;
import de.mewin.betterquestsaddons.BetterQuestsAddonsPlugin;
import de.mewin.betterquestsaddons.vault.Providers;
import de.mewin.betterquestsaddons.vault.requirements.MoneyRequirementFactory;
import de.mewin.betterquestsaddons.vault.requirements.PermissionRequirementFactory;
import de.mewin.betterquestsaddons.vault.rewards.MoneyRewardFactory;
import de.mewin.betterquestsaddons.vault.rewards.PermissionRewardFactory;

/**
 *
 * @author mewin
 */
public class VaultAddon extends Addon
{
    public VaultAddon(BetterQuestsAddonsPlugin plugin)
    {
        super(plugin);
    }

    @Override
    public void onEnable()
    {
        FactoryRegistry factoryRegistry = betterQuests.getFactoryRegistry();
        Providers.setupVault();

        if (Providers.hasEconomy())
        {
            factoryRegistry.tryRegisterRequirementFactory(MoneyRequirementFactory.MY_ID, new MoneyRequirementFactory());
            factoryRegistry.tryRegisterRequirementFactory(PermissionRequirementFactory.MY_ID, new PermissionRequirementFactory());
            factoryRegistry.tryRegisterRewardFactory(MoneyRewardFactory.MY_ID, new MoneyRewardFactory());
        }

        if (Providers.hasPermission())
        {
            factoryRegistry.tryRegisterRewardFactory(PermissionRewardFactory.MY_ID, new PermissionRewardFactory());
        }
    }
}
