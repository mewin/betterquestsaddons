/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.addons;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquestsaddons.BetterQuestsAddonsPlugin;

/**
 *
 * @author mewin
 */
public class Addon
{
    protected final BetterQuestsPlugin betterQuests;
    protected final BetterQuestsAddonsPlugin betterQuestsAddons;

    protected Addon(BetterQuestsAddonsPlugin plugin)
    {
        betterQuestsAddons = plugin;
        betterQuests = plugin.getBasePlugin();
    }

    public void onEnable()
    {

    }

    public void onDisable()
    {

    }
}
