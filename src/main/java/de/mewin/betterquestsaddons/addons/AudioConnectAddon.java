/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.addons;

import de.mewin.betterquests.quest.FactoryRegistry;
import de.mewin.betterquestsaddons.BetterQuestsAddonsPlugin;
import de.mewin.betterquestsaddons.audioconnect.rewards.AudioRewardFactory;

/**
 *
 * @author mewin
 */
public class AudioConnectAddon extends Addon
{

    public AudioConnectAddon(BetterQuestsAddonsPlugin plugin)
    {
        super(plugin);
    }

    @Override
    public void onEnable()
    {
        FactoryRegistry factoryRegistry = betterQuests.getFactoryRegistry();

        factoryRegistry.tryRegisterRewardFactory(AudioRewardFactory.MY_ID, new AudioRewardFactory());
    }
}
