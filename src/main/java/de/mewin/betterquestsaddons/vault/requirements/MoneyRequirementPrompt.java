/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.requirements;

import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.quest.extra.DataType;
import de.mewin.betterquests.quest.extra.KnownSetting;
import de.mewin.betterquests.requirements.conversation.RequirementPrompt;

import java.util.ArrayList;
import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.DoubleAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class MoneyRequirementPrompt extends RequirementPrompt
{
    private final MoneyRequirement moneyRequirement;

    public MoneyRequirementPrompt(MoneyRequirement requirement, RequirementsHolder holder, MenuPrompt parentPrompt)
    {
        super(requirement, holder, parentPrompt);

        this.moneyRequirement = requirement;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-vault-money-amount", moneyRequirement.getAmount()),
                new DoubleAcceptorPrompt(this, "prompt-addons-vault-money-amount", moneyRequirement));

        super.fillOptions(receiver);
    }

    @Override
    protected List<KnownSetting> getKnownSettings()
    {
        ArrayList<KnownSetting> settings = new ArrayList<>(super.getKnownSettings());
        settings.add(new KnownSetting(MoneyRequirement.TAKE_MONEY, DataType.BOOLEAN));
        return settings;
    }
}
