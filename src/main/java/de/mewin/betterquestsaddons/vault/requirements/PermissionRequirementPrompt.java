/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.requirements;

import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.requirements.conversation.RequirementPrompt;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PermissionRequirementPrompt extends RequirementPrompt
{
    private final PermissionRequirement permissionRequirement;

    public PermissionRequirementPrompt(PermissionRequirement requirement, RequirementsHolder holder, MenuPrompt parentPrompt)
    {
        super(requirement, holder, parentPrompt);

        permissionRequirement = requirement;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-vault-permission", permissionRequirement.getPermission()),
                new StringAcceptorPrompt(this, "prompt-addons-vault-permission", permissionRequirement));

        super.fillOptions(receiver);
    }
}
