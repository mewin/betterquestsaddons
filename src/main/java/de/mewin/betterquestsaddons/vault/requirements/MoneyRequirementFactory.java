/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.requirements;

import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.quest.factory.RequirementFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class MoneyRequirementFactory extends RequirementFactory
{
    @Override
    public Requirement createNew(RequirementsHolder holder)
    {
        MoneyRequirement requirement = new MoneyRequirement(holder);
        holder.addRequirement(requirement);
        return requirement;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-vault-requirement-money-factory");
    }

    @Override
    public Requirement fromYaml(RequirementsHolder holder, Map<String, Object> yaml) throws IOException
    {
        double amount = 0.0;
        Object tmp;

        if ((tmp = yaml.get("amount")) != null && (tmp instanceof Number)) {
            amount = ((Number) tmp).doubleValue();
        }

        return new MoneyRequirement(holder, amount);
    }

    @Override
    public Map<String, Object> toYaml(Requirement requirement) throws IOException
    {
        MoneyRequirement moneyRequirement = (MoneyRequirement) requirement;

        HashMap<String, Object> map = new HashMap<>();

        map.put("amount", moneyRequirement.getAmount());

        return map;
    }

    public static final String MY_ID = "addons-vault-money";
}
