/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.requirements;

import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquestsaddons.vault.Providers;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PermissionRequirement extends Requirement implements StringAcceptor
{
    private String permission;

    public PermissionRequirement(RequirementsHolder holder)
    {
        super(holder);

        permission = "";
    }

    public PermissionRequirement(RequirementsHolder holder, String permission)
    {
        super(holder);
        this.permission = permission;
    }

    public String getPermission()
    {
        return permission;
    }

    public void setPermission(String permission)
    {
        this.permission = permission;
    }

    @Override
    public boolean testRequirement(Player player)
    {
        Permission perm = Providers.getPermission();

        return perm.has(player, this.permission);
    }

    @Override
    public String getFailMessage(Player player)
    {
        return CHAT_HELPER.getMessage(player, "addons-vault-requirement-permission-missing", permission);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-vault-requirement-permission", permission);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RequirementsHolder base, CommandSender sender)
    {
        return new PermissionRequirementPrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return PermissionRequirementFactory.MY_ID;
    }

    @Override
    public String getString()
    {
        return permission;
    }

    @Override
    public boolean setString(String value)
    {
        permission = value;
        return true;
    }
}
