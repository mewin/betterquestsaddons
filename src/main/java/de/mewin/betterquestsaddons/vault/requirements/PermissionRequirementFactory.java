/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.requirements;

import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.quest.factory.RequirementFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PermissionRequirementFactory extends RequirementFactory
{
    @Override
    public Requirement createNew(RequirementsHolder holder)
    {
        PermissionRequirement requirement = new PermissionRequirement(holder);
        holder.addRequirement(requirement);
        return requirement;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-vault-requirement-permission-factory");
    }

    @Override
    public Requirement fromYaml(RequirementsHolder holder, Map<String, Object> yaml) throws IOException
    {
        String permission = "";
        Object tmp;

        if ((tmp = yaml.get("permission")) != null) {
            permission = tmp.toString();
        }

        return new PermissionRequirement(holder, permission);
    }

    @Override
    public Map<String, Object> toYaml(Requirement requirement) throws IOException
    {
        PermissionRequirement permissionRequirement = (PermissionRequirement) requirement;

        HashMap<String, Object> map = new HashMap<>();

        map.put("permission", permissionRequirement.getPermission());

        return map;
    }

    public static final String MY_ID = "addons-vault-permission";
}
