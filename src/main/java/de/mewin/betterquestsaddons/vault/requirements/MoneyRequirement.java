/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.requirements;

import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquestsaddons.vault.Providers;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.DoubleAcceptor;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class MoneyRequirement extends Requirement implements DoubleAcceptor
{
    private double amount;

    public MoneyRequirement(RequirementsHolder holder)
    {
        super(holder);
        amount = 0.0;
    }

    public MoneyRequirement(RequirementsHolder holder, double amount)
    {
        super(holder);
        this.amount = amount;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    @Override
    public boolean testRequirement(Player player)
    {
        Economy economy = Providers.getEconomy();
        return economy.getBalance(player) >= amount;
    }

    @Override
    public String getFailMessage(Player player)
    {
        Economy economy = Providers.getEconomy();

        return CHAT_HELPER.getMessage(player, "addons-vault-requirement-money-missing", economy.format(amount));
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-vault-requirement-money", amount);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RequirementsHolder holder, CommandSender sender)
    {
        return new MoneyRequirementPrompt(this, holder, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return MoneyRequirementFactory.MY_ID;
    }

    @Override
    public void onApply(Player player)
    {
        if (getSettings().getBoolean(TAKE_MONEY, false)) {
            Economy economy = Providers.getEconomy();
            economy.withdrawPlayer(player, amount);
        }
    }

    @Override
    public double getDouble()
    {
        return amount;
    }

    @Override
    public boolean setDouble(double value)
    {
        if (value < 0.0) {
            return false;
        }

        amount = value;

        return true;
    }

    public static final String TAKE_MONEY = "take-money";
}
