/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

/**
 *
 * @author mewin
 */
public class Providers
{
    private static Economy economy;
    private static Permission permission;
//    private static Chat chat; // not needed (yet)

    public static Economy getEconomy()
    {
        return economy;
    }

    public static Permission getPermission()
    {
        return permission;
    }

    public static boolean hasEconomy()
    {
        return economy != null;
    }

    public static boolean hasPermission()
    {
        return permission != null;
    }

//    public static Chat getChat()
//    {
//        return chat;
//    }

    public static void setupVault()
    {
        RegisteredServiceProvider<Economy> econProvider = Bukkit.getServicesManager().getRegistration(Economy.class);
        if (econProvider != null) {
            economy = econProvider.getProvider();
        }

        RegisteredServiceProvider<Permission> permProvider = Bukkit.getServicesManager().getRegistration(Permission.class);
        if (permProvider != null) {
            permission = permProvider.getProvider();
        }
    }
}
