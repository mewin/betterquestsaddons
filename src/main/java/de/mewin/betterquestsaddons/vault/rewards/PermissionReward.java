/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.rewards;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquestsaddons.vault.Providers;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PermissionReward extends Reward implements StringAcceptor
{
    private String permission;

    public PermissionReward(RewardsHolder holder)
    {
        super(holder);
        permission = "";
    }

    public PermissionReward(RewardsHolder holder, String permission)
    {
        super(holder);
        this.permission = permission;
    }

    public String getPermission()
    {
        return permission;
    }

    public void setPermission(String permission)
    {
        this.permission = permission;
    }

    @Override
    public void giveReward(Player player)
    {
        Permission perm = Providers.getPermission();
        perm.playerAdd(null, player, permission);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-vault-reward-permission", permission);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RewardsHolder holder, CommandSender sender)
    {
        return new PermissionRewardPrompt(this, holder, parentPrompt);
    }

    @Override
    public String getMessage(Player player)
    {
        return CHAT_HELPER.getMessage(player, "addons-vault-reward-permission-message", permission);
    }

    @Override
    public String getFactoryId()
    {
        return PermissionRewardFactory.MY_ID;
    }

    @Override
    public String getString()
    {
        return permission;
    }

    @Override
    public boolean setString(String value)
    {
        permission = value;
        return true;
    }
}
