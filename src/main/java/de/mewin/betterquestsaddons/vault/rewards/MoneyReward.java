/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.rewards;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquestsaddons.vault.Providers;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.DoubleAcceptor;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class MoneyReward extends Reward implements DoubleAcceptor
{
    private double amount;

    public MoneyReward(RewardsHolder holder)
    {
        super(holder);
        this.amount = 0.0;
    }

    public MoneyReward(RewardsHolder holder, double amount)
    {
        super(holder);
        this.amount = amount;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    @Override
    public void giveReward(Player player)
    {
        Economy economy = Providers.getEconomy();
        economy.depositPlayer(player, amount);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-vault-reward-money", amount);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RewardsHolder holder, CommandSender sender)
    {
        return new MoneyRewardPrompt(this, holder, parentPrompt);
    }

    @Override
    public String getMessage(Player player)
    {
        Economy economy = Providers.getEconomy();
        String amountString = economy.format(amount);

        return CHAT_HELPER.getMessage(player, "addons-vault-reward-money-message", amountString);
    }

    @Override
    public String getFactoryId()
    {
        return MoneyRewardFactory.MY_ID;
    }

    @Override
    public double getDouble()
    {
        return amount;
    }

    @Override
    public boolean setDouble(double value)
    {
        if (value < 0.0) {
            return false;
        }
        amount = value;
        return true;
    }
}
