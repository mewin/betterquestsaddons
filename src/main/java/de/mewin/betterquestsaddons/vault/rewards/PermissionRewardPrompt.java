/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.rewards;

import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.rewards.conversation.RewardPrompt;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PermissionRewardPrompt extends RewardPrompt
{
    private final PermissionReward permissionReward;

    public PermissionRewardPrompt(PermissionReward reward, RewardsHolder holder, MenuPrompt parentPrompt)
    {
        super(reward, holder, parentPrompt);
        this.permissionReward = reward;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-vault-permission", permissionReward.getPermission()),
                new StringAcceptorPrompt(this, "prompt-addons-vault-permission", permissionReward));

        super.fillOptions(receiver);
    }
}
