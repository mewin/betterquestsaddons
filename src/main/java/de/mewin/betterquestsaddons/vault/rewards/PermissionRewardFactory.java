/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.rewards;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.quest.factory.RewardFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PermissionRewardFactory extends RewardFactory
{
    @Override
    public Reward createNew(RewardsHolder holder)
    {
        PermissionReward permissionReward = new PermissionReward(holder);
        holder.addReward(permissionReward);
        return permissionReward;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-vault-reward-permission-factory");
    }

    @Override
    public Reward fromYaml(RewardsHolder holder, Map<String, Object> yaml) throws IOException
    {
        String permission = "";

        Object tmp;
        if ((tmp = yaml.get("permission")) != null) {
            permission = tmp.toString();
        }

        return new PermissionReward(holder, permission);
    }

    @Override
    public Map<String, Object> toYaml(Reward reward) throws IOException
    {
        PermissionReward permissionReward = (PermissionReward) reward;

        HashMap<String, Object> map = new HashMap<>();
        map.put("permission", permissionReward.getPermission());

        return map;
    }

    public static final String MY_ID = "addons-vault-permission";
}
