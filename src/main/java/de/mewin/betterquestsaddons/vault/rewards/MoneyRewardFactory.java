/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.rewards;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.quest.factory.RewardFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class MoneyRewardFactory extends RewardFactory
{
    @Override
    public Reward createNew(RewardsHolder holder)
    {
        MoneyReward reward = new MoneyReward(holder);
        holder.addReward(reward);
        return reward;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "addons-vault-reward-money-factory");
    }

    @Override
    public Reward fromYaml(RewardsHolder holder, Map<String, Object> yaml) throws IOException
    {
        Object tmp;
        if ((tmp = yaml.get("amount")) != null && tmp instanceof Number) {
            return new MoneyReward(holder, ((Number) tmp).doubleValue());
        }
        return new MoneyReward(holder);
    }

    @Override
    public Map<String, Object> toYaml(Reward reward) throws IOException
    {
        MoneyReward moneyReward = (MoneyReward) reward;

        HashMap<String, Object> map = new HashMap<>();
        map.put("amount", moneyReward.getAmount());
        return map;
    }

    public static final String MY_ID = "addons-vault-money";
}
