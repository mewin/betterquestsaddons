/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquestsaddons.vault.rewards;

import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.rewards.conversation.RewardPrompt;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.DoubleAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class MoneyRewardPrompt extends RewardPrompt
{
    private final MoneyReward moneyReward;

    public MoneyRewardPrompt(MoneyReward moneyReward, RewardsHolder holder, MenuPrompt parentPrompt)
    {
        super(moneyReward, holder, parentPrompt);
        this.moneyReward = moneyReward;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-addons-vault-money-amount", moneyReward.getAmount()),
                new DoubleAcceptorPrompt(this, "prompt-addons-vault-money-amount", moneyReward));
        super.fillOptions(receiver);
    }
}
