# BetterQuestsAddons

Plugin that adds integration for various third party plugins to [BetterQuests](https://gitlab.com/mewin/betterquests). Adds different kinds of triggers, objectives, requierments and rewards.

Currently the following plugins are supported:

 * [AudioConnect](https://www.spigotmc.org/resources/audioconnect.40339/) - Play sounds on quest/stage completion.
 * [Citizens](https://www.spigotmc.org/resources/citizens.13811/) - Get quests from your NPCs or talk to them as an objective.
 * [Vault](https://www.spigotmc.org/resources/vault.34315/) - Give your players money as a reward or require money to start a quest.
 * [WorldGuard](https://dev.bukkit.org/projects/worldguard) - Send your players to WorldGuard regions or require them to be inside them.

## Getting Started

Just clone the repository and compile using maven (`mvn package` to create jar files).

### Prerequisites

You will need to install maven and any JDK > 8.

## Deployment

To install this to a Bukkit/Spigot server simply copy the created jar file (target/BetterQuestsAddons-x.x-SNAPSHOT.jar) to the plugins folder. Obviously [BetterQuests](https://gitlab.com/mewin/betterquests) is also required.

Parts of the WorldGuard integration also require [WorldGuard Region Events](https://gitlab.com/mewin/WorldGuard-Region-Events) to function.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

You can create issues [on GitLab](https://gitlab.com/mewin/betterquestsaddons/issues).

## Versioning

For the versions available, see the [tags on this repository](https://gitlab.com/mewin/betterquestsaddons/tags). 

## Authors

* **Patrick Wuttke** - *Initial work* - [mewin](https://mewin.de)

See also the list of [contributors](https://gitlab.com/mewin/betterquestsaddons/graphs/master) who participated in this project.

## License

This project is published under the terms of the GNU General Public License v3.0. See the LICENSE file for more information.

## Acknowledgments

* the Bukkit and Spigot devs
* the team of [Avalunia](https://avalunia.de) who provided great ideas and feedback
